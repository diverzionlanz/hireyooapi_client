const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const cookieSession = require('cookie-session');
const passport = require('passport');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
require('./services/passport');

const db = require('./db')
const userRouter = require('./routes/users-router')
const clientRouter = require('./routes/client-router')
const skillsRouter = require('./routes/skills-router')


const app = express()
const apiPort = 5000


app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())


const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: "HireYoo Client API",
        version: '1.0.0',
      },
    },
    apis: ["index.js"],
  };

  const swaggerDocs = swaggerJsDoc(swaggerOptions);
  app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));
  
  

db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.get('/', (req, res) => {
    res.send('Hello World!')
})

/**
 * @swagger
 * /user/signup:
*   post:
*      description: Used to register user
*      tags:
*          - Client
*      parameters:
*          - in: body
*            name: User
*            description: User data
*            schema:
*              type: object
*              required:
*                 - firstName
*                 - lastName
*                 - email
*                 - provider
*                 - password
*              properties:
*                  firstName:
*                      type: string
*                      minLength: 1
*                      maxLength: 45
*                      example: Lanz
*                  middleName:
*                      type: string
*                      minLength: 1
*                      maxLength: 45
*                      example: Bocado
*                  lastName:
*                      type: string
*                      minLength: 1
*                      maxLength: 45
*                      example: Ruiz
*                  email:
*                      type: string
*                      minLength: 1
*                      maxLength: 100
*                      example: Lanzruiz101@gmail.com
*                  provider:
*                      type: string
*                      minLength: 1
*                      maxLength: 100
*                      example: google
*                  password:
*                      type: string
*                      minLength: 1
*                      maxLength: 45
*                      example: abcd
*      responses:
*          '200':
*              description: Resource added successfully
*          '500':
*              description: Internal server error
*          '400':
*              description: Bad request
*/


/**
 * @swagger
 * /user/login:
*   post:
*      description: Used to login user
*      tags:
*          - Client
*      parameters:
*          - in: body
*            name: User
*            description: User data
*            schema:
*              type: object
*              required:
*                 - email
*                 - password
*              properties:
*                  email:
*                      type: string
*                      minLength: 1
*                      maxLength: 100
*                      example: Lanzruiz101@gmail.com
*                  password:
*                      type: string
*                      minLength: 1
*                      maxLength: 45
*                      example: abcd
*      responses:
*          '200':
*              description: Resource added successfully
*          '500':
*              description: Internal server error
*          '400':
*              description: Bad request
*/


/**
 * @swagger
 * /user/mobile/google/signin:
*   post:
*      description: Used to login user
*      tags:
*          - Users
*      parameters:
*          - in: body
*            name: User
*            description: User data
*            schema:
*              type: object
*              required:
*                 - email
*              properties:
*                  email:
*                      type: string
*                      minLength: 1
*                      maxLength: 100
*                      example: Lanzruiz101@gmail.com
*      responses:
*          '200':
*              description: Resource added successfully
*          '500':
*              description: Internal server error
*          '400':
*              description: Bad request
*/


/**
 * @swagger
 * /user/mobile/facebook/signin:
*   post:
*      description: Used to login user
*      tags:
*          - Client
*      parameters:
*          - in: body
*            name: User
*            description: User data
*            schema:
*              type: object
*              required:
*                 - email
*              properties:
*                  email:
*                      type: string
*                      minLength: 1
*                      maxLength: 100
*                      example: Lanzruiz101@gmail.com
*      responses:
*          '200':
*              description: Resource added successfully
*          '500':
*              description: Internal server error
*          '400':
*              description: Bad request
*/



/**
 * @swagger
 * /user/{id}:
*   get:
*      description: Find the user information
*      tags:
*          - Client
*      parameters:
*          - name: "id"
*            in: "path"
*            description: "Id is that need the user to be fetch"
*            required: true
*            type: "string"
*            maximum: 10.0
*            minimum: 1.0
*            format: "int64" 
*      responses:
*          '200':
*              description: Resource added successfully
*          '500':
*              description: Internal server error
*          '400':
*              description: Bad request
*/


/**
 * @swagger
 * /user/token:
*   post:
*      description: Use to generate new access token
*      tags:
*          - Client
*      parameters:
*          - in: body
*            name: User
*            description: User data
*            schema:
*              type: object
*              required:
*                 - refreshToken
*              properties:
*                  refreshToken:
*                      type: string
*                      minLength: 1
*                      maxLength: 500
*                      example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTGFuenJ1aXpkZXNpZ25zOUBnbWFpbC5jb20iLCJwYXNzd29yZCI6IkJvdW5jZTEyMzQhIiwiaWF0IjoxNjMyMzg0OTA1LCJleHAiOjE2MzIzODQ5MjV9.zB8qiAPxgmhjyhcb5NObTreGQGDOfvV60WS1sa_3ZIY
*      responses:
*          '200':
*              description: Resource added successfully
*          '500':
*              description: Internal server error
*          '400':
*              description: Bad request
*/

/**
 * @swagger
 * /user/:
*   delete:
*      description: Delete the user in the database
*      tags:
*          - Client
*      parameters:
*          - name: "id"
*            in: body
*            description: "Id is that need the user to be fetch"
*            required: true
*            type: "string"
*            maximum: 10.0
*            minimum: 1.0
*            format: "int64" 
*          - name: "Authorization"
*            in: "header"
*            description: "Jwt Authenticatioin"
*            required: true
*            type: "string"
*            maximum: 100.0
*            minimum: 1.0
*            format: "int64" 
*      responses:
*          '200':
*              description: Resource deleted successfully
*          '500':
*              description: Internal server error
*          '400':
*              description: Bad request
*/


/**
 * @swagger
 * /user/:
*   put:
*      description: Use to change the information of the user
*      tags:
*          - Client
*      parameters:
*          - name: "Authorization"
*            in: "header"
*            description: "Jwt Authenticatioin"
*            required: true
*            type: "string"
*            maximum: 100.0
*            minimum: 1.0
*            format: "int64" 
*          - in: body
*            name: User
*            description: User data
*            schema:
*              type: object
*              required:
*                 - name
*                 - email
*                 - password
*                 - id
*              properties:
*                  id:
*                      type: string
*                      minLength: 1
*                      maxLength: 100
*                      example: 6145a487f4585bdffdfef21c
*                  name:
*                      type: string
*                      minLength: 1
*                      maxLength: 45
*                      example: Lanz
*                  email:
*                      type: string
*                      minLength: 1
*                      maxLength: 100
*                      example: Lanzruiz101@gmail.com
*                  password:
*                      type: string
*                      minLength: 1
*                      maxLength: 45
*                      example: abcd
*      responses:
*          '200':
*              description: Resource added successfully
*          '500':
*              description: Internal server error
*          '400':
*              description: Bad request
*/

app.use('/user', userRouter)
app.use('/client', clientRouter)
app.use('/skills', skillsRouter)

app.use(cookieSession({
    name: 'google-auth-session',
    keys: ['key1', 'key2']
  }))
  
  const isLoggedIn = (req, res, next) => {
      if (req.user) {
          next();
      } else {
          res.sendStatus(401);
      }
  }
  
  app.use(passport.initialize());
  app.use(passport.session());
  
  const port = process.env.PORT || 5000


app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`))