const express = require('express')
require('dotenv').config()


const UserCtrl = require('../controllers/users-ctrl')
const AuthCtrl = require('../controllers/auth-ctrl')
const VicesCtrl = require('../controllers/vices-ctrl')
const MetricsCtrl = require('../controllers/metrics-ctrl')
const jwt = require('jsonwebtoken')

const router = express.Router()

const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      console.log(err)
      if (err) return res.sendStatus(403)
      req.user = user
      next()
    })
}


router.post('/', authenticateToken, MetricsCtrl.createMetrics)
// router.get('/all/:id', authenticateToken, VicesCtrl.getAllVicesByUserId)
// router.get('/:id', authenticateToken, VicesCtrl.getVicesById)
// router.delete('/', authenticateToken, VicesCtrl.deleteVicesById)
// router.put('/', authenticateToken, VicesCtrl.updateVicesById)
// router.get('/:id', UserCtrl.getUserById)
// router.post('/signup', UserCtrl.signUp)
// router.post('/login', AuthCtrl.login)
// router.delete('/', authenticateToken, UserCtrl.deleteUser)
// router.put('/', authenticateToken, UserCtrl.updateUser)



module.exports = router