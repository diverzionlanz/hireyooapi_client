const express = require('express')
require('dotenv').config()
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');


const UserCtrl = require('../controllers/users-ctrl')
const ClientCtrl = require('../controllers/client-ctrl');
const AuthCtrl = require('../controllers/auth-ctrl')
const jwt = require('jsonwebtoken')


const router = express.Router()

const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      console.log(err)
      if (err) return res.sendStatus(403)
      req.user = user
      next()
    })
}



const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "HireYoo API",
      version: '1.0.0',
    },
  },
  apis: ["index.js"],
};





router.get('/:clientId', ClientCtrl.getClientByclientId)
router.post('/signup', ClientCtrl.signUp)
router.post('/login', ClientCtrl.login)
router.post('/token', ClientCtrl.token)
router.delete('/', authenticateToken, ClientCtrl.deleteClient)
router.put('/', authenticateToken, ClientCtrl.updateClientByclientId)



module.exports = router