const express = require('express')
require('dotenv').config()


const UserCtrl = require('../controllers/users-ctrl')
const AuthCtrl = require('../controllers/auth-ctrl')
const VicesCtrl = require('../controllers/vices-ctrl')
const SkillsCtrl = require('../controllers/skills-ctrl')
const jwt = require('jsonwebtoken')

const router = express.Router()

const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      console.log(err)
      if (err) return res.sendStatus(403)
      req.user = user
      next()
    })
}


router.post('/:userId', authenticateToken, SkillsCtrl.createSkills)
router.get('/all/:id', authenticateToken, SkillsCtrl.getAllSkillsByUserId)
router.get('/:id', authenticateToken, SkillsCtrl.getSkillsById)
router.delete('/', authenticateToken, SkillsCtrl.deleteSkillsById)
router.put('/', authenticateToken, SkillsCtrl.updateSkillsById)
router.put('/:userId', authenticateToken, SkillsCtrl.updateSkillsByUserId)
// router.get('/:id', UserCtrl.getUserById)
// router.post('/signup', UserCtrl.signUp)
// router.post('/login', AuthCtrl.login)
// router.delete('/', authenticateToken, UserCtrl.deleteUser)
// router.put('/', authenticateToken, UserCtrl.updateUser)



module.exports = router