const Vices = require('../models/vices-model')
const bcrypt = require("bcrypt");
const mongoose = require('mongoose')
require('dotenv').config()

createSkills = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a skills',
        })
    }



    console.log(Object.keys(req.body).length)
    for(var i = 0; i < Object.keys(req.body).length; i++) {

            console.log(req.body[i].skillsName)

            const skills = new Skills({
                userId: req.params.userId,
                skillsName: req.body[i].skillsName,
                skillsRate: req.body[i].skillsRate,
                skillsCategory: req.body[i].skillsCategory,

            })

            if (!skills) {
                return res.status(400).json({ success: false, error: err })
            }

            skills
                .save()
                .then(() => {
                    return res.status(201).json({
                        success: true,
                        id: skills._id,
                        message: 'skills user created!',
                    })
                })
                .catch(error => {
                    return res.status(400).json({
                        error,
                        message: 'skills user not created!',
                    })
                })
            //
    }

}


getAllSkillsByUserId = async (req, res) => {

    await Vices.find({ userId: req.params.id }, (err, vices) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!vices.length) {
            return res
                .status(404)
                .json({ success: false, error: `Vices not found` })
        }
        return res.status(200).json({ success: true, data: vices })
    }).catch(err => console.log(err))
}


getSkillsById = async (req, res) => {
    await Vices.findOne({ _id: req.params.id }, (err, vices) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!vices) {
            return res
                .status(404)
                .json({ success: false, error: `User not found` })
        }
        const vicesData = {
            id: vices._id,
            vicesStatement: vices.viceStatement, 
            userId: vices.userId,
            createdAt: vices.createdAt,
            updatedAt: vices.updatedAt,
        }
        return res.status(200).json({ success: true, data: vicesData })
    }).catch(err => console.log(err))
}

deleteSkillsById = async (req, res) => {
    const userId = req.body.id
    await Vices.findOneAndDelete({ _id: req.body.id  }, (err, vices) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!vices) {
            return res
                .status(404)
                .json({ success: false, error: `Vices not found` })
        }

        return res.status(200).json({ success: true, message: "Vices has been deleted!" })
    }).catch(err => console.log(err))
}


updateSkillsById = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    const vicesData = {
        viceStatement: req.body.viceStatement
    }
    
    Vices.updateOne({_id: req.body.id}, vicesData).then(
        () => {
          res.status(201).json({
            message: 'Vices updated successfully!'
          });
        }
      ).catch(
        (error) => {
            res.status(400).json({
              error: error
            });
          }
      );

}


updateSkillsByUserId = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }
    
    console.log(Object.keys(req.body).length)
    for(var i = 0; i < Object.keys(req.body).length; i++) {
        
        console.log(req.body[i].viceStatement)
        const vicesData = {
           viceStatement: req.body[i].viceStatement
        }
        Vices.updateOne({_id: req.body[i].id}, vicesData).then(
            () => {
            res.status(201).json({
                message: 'Vices updated successfully!'
            });
            }
        ).catch(
            (error) => {
                res.status(400).json({
                error: error
                });
            }
        );
    }

}

module.exports = {
    createSkills,
    getAllSkillsByUserId,
    getSkillsById,
    deleteSkillsById,
    updateSkillsById,
    updateSkillsByUserId
}