const Metrics = require('../models/metrics-model')
const bcrypt = require("bcrypt");
const mongoose = require('mongoose')
require('dotenv').config()

createMetrics = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a person',
        })
    }

    const metrics = new Metrics(body)

    if (!metrics) {
        return res.status(400).json({ success: false, error: err })
    }

    metrics
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: metrics._id,
                message: 'Metrics created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'Metrics not created!',
            })
        })
}

getAllMetricsByUserId = async (req, res) => {

    await Metrics.find({ userId: req.params.id }, (err, metrics) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!metrics.length) {
            return res
                .status(404)
                .json({ success: false, error: `Vices not found` })
        }
        return res.status(200).json({ success: true, data: metrics })
    }).catch(err => console.log(err))
}


getMetricsById = async (req, res) => {
    await Metrics.findOne({ _id: req.params.id }, (err, metrics) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!metrics) {
            return res
                .status(404)
                .json({ success: false, error: `User not found` })
        }
        const metricsData = {
            id: metrics._id,
            chest: metrics.chest,
            waist: metrics.waist,
            hips: metrics.hips,
            gender: metrics.gender,
            userId: vices.userId,
            createdAt: vices.createdAt,
            updatedAt: vices.updatedAt,
        }
        return res.status(200).json({ success: true, data: metricsData })
    }).catch(err => console.log(err))
}

deleteMetricsById = async (req, res) => {
    const userId = req.body.id
    await Metrics.findOneAndDelete({ _id: req.body.id  }, (err, vices) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!metrics) {
            return res
                .status(404)
                .json({ success: false, error: `Vices not found` })
        }

        return res.status(200).json({ success: true, message: "Metric has been deleted!" })
    }).catch(err => console.log(err))
}


updateMetricsById = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    const metricsData = {
        chest: metrics.chest,
        waist: metrics.waist,
        hips: metrics.hips,
        gender: metrics.gender,
    }
    
    Vices.updateOne({_id: req.body.id}, metricsData).then(
        () => {
          res.status(201).json({
            message: 'Metrics updated successfully!'
          });
        }
      ).catch(
        (error) => {
            res.status(400).json({
              error: error
            });
          }
      );

}

getMetricsChestByUserId = async (req, res) => {
    await Metrics.findOne({ _id: req.params.id }, (err, metrics) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!metrics) {
            return res
                .status(404)
                .json({ success: false, error: `User not found` })
        }
        const metricsChestData = {
            id: metrics._id,
            chest: metrics.chest,
            createdAt: vices.createdAt,
            updatedAt: vices.updatedAt,
        }
        return res.status(200).json({ success: true, data: metricsChestData })
    }).catch(err => console.log(err)) 
}

getMetricsWaistByUserId = async (req, res) => {
    await Metrics.findOne({ _id: req.params.id }, (err, metrics) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!metrics) {
            return res
                .status(404)
                .json({ success: false, error: `User not found` })
        }
        const metricsWaistData = {
            id: metrics._id,
            waist: metrics.waist,
            createdAt: vices.createdAt,
            updatedAt: vices.updatedAt,
        }
        return res.status(200).json({ success: true, data: metricsChestData })
    }).catch(err => console.log(err)) 
}

getMetricsHipsByUserId = async (req, res) => {
    await Metrics.findOne({ _id: req.params.id }, (err, metrics) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!metrics) {
            return res
                .status(404)
                .json({ success: false, error: `User not found` })
        }
        const metricsWaistData = {
            id: metrics._id,
            hips: metrics.hips,
            createdAt: vices.createdAt,
            updatedAt: vices.updatedAt,
        }
        return res.status(200).json({ success: true, data: metricsChestData })
    }).catch(err => console.log(err)) 
}

module.exports = {
    createMetrics,
    getAllMetricsByUserId,
    getMetricsById,
    deleteMetricsById,
    updateMetricsById,
    getMetricsChestByUserId,
    getMetricsWaistByUserId,

}