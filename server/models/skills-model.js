const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Skills = new Schema(
    {
        skillsName: { type: String, required: true },
        skillsCategory: { type: Number, required: true },
        status: { type: Number, required: true },
    },
    { timestamps: true },
)

module.exports = mongoose.model('skills', Skills)