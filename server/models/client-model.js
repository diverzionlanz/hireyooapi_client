const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Client = new Schema(
    {
        firstName: { type: String, required: true },
        middleName: { type: String, required: true },
        lastName: { type: String, required: true },
        email:{ type: String, required: true },
        status: { type: String, default: 1},
        password:{ type: String, required: true },
        provider:{ type: String, required: true },
        
        
    },
    { timestamps: true },
)

module.exports = mongoose.model('client', Client)