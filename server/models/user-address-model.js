const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserAddress = new Schema(
    {
        userId: { type: String, required: true },
        unit:{ type: String, required: true },
        street:{ type: String, required: true },
        city:{ type: String, required: true },
        country:{ type: String, required: true },
        
    },
    { timestamps: true },
)

module.exports = mongoose.model('userAddress', UserAddress)