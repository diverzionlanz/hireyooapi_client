const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Vices = new Schema(
    {
        viceStatement: { type: String, required: false },
        userId: { type: String, required: true },
        
    },
    { timestamps: true },
)

module.exports = mongoose.model('vices', Vices)