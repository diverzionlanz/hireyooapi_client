const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = new Schema(
    {
        firstName: { type: String, required: true },
        middleName: { type: String, required: false },
        lastName: { type: String, required: true },
        email:{ type: String, required: true },
        status: { type: String, default: 1 },
        password:{ type: String, required: true },
        provider:{ type: String, required: true },
        
        
    },
    { timestamps: true },
)

module.exports = mongoose.model('user', User)